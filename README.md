# Wardrobify

Team:

* Isaiah- Hats microservice
* Kevinjeet- Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
Created Models for Shoes and BinVo
Created encoders and api views for Shoe list
The Shoe model uses the BinVo as a foreign key
BinVo uses the ModelEncoder from the wardrob microsevice.

## Hats microservice
Started this project by creating a Hat model entity withiin this microservice as well as a Location VO in order to prevent immutability. To enable multiple hats to exist in a single location, I included the location as a foreign key in the Hat model.To integrate the wardrobe microservice, I included a poller that often requests data from the wardrobe API.
