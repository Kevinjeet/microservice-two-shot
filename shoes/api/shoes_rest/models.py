from django.db import models

class BinVo(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href= models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number} / {self.bin_size}"
    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=5000, null=True)
    bin = models.ForeignKey(
        BinVo,
        related_name="bin",
        on_delete=models.CASCADE
    )
