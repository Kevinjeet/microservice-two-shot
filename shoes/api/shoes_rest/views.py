from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVo
import json

class BinVOEncoder(ModelEncoder):
    model = BinVo
    properties = [
        "closet_name",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin",
        "id",

    ]
    encoders = {
        "bin": BinVOEncoder()
    }
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }


#
@require_http_methods(["GET", "POST"])
def shoe_list(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            print(bin_href, "------------")
            bin = BinVo.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_detail(request, id):
    if request.method == "GET":
        try:
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    if request.method == "PUT":
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=id)
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
