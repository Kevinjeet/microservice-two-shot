import React, {useEffect, useState} from 'react'

function ShoeForm() {
    const [modelName, setModelName] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [bin, setBin] = useState('')
    const [bins, setBins] = useState([])

    const modelNameChange = (event) => {
        setModelName(event.target.value)
    }
    const manufacturerChange = (event) => {
        setManufacturer(event.target.value)
    }
    const colorChange = (event) => {
        setColor(event.target.value)
    }
    const pictureUrlChange = (event) => {
        setPictureUrl(event.target.value)
    }
    const binChange = (event) => {
        setBin(event.target.value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data= {}
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        console.log(data)
        const shoeUrl = "http://localhost:8080/api/shoes/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            setModelName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
        window.location.reload();
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={modelNameChange} placeholder="Model name"
              required type="text"
              name="model_name" id="model_name"
              className="form-control" value={modelName} />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={manufacturerChange} placeholder="Manufacturer"
              required type="text"
              name="manufacturer"
              id="manufacturer"
              className="form-control" value={manufacturer} />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={colorChange} placeholder="Color"
              required type="text"
              name="color" id="color"
              className="form-control" value={color} />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={pictureUrlChange} placeholder="Picture Url"
              required type="text"
              name="picture_url" id="picture_url"
              className="form-control" value={pictureUrl} />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={binChange} required name="bin"
              id="bin"
              className="form-select" value={bin} >
                <option value="">Choose a bin</option>
                {bins?.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>



    );
}
export default ShoeForm
