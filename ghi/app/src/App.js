import React, { useState, useEffect } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatsList from './HatList';
import HatForm  from './HatForm'

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <>

    <BrowserRouter>
      <Nav />
      <div className="container">


        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            {/* <Route path="" element={<ShoeForm />} /> */}
            {/* <ShoeList shoes={props.shoes} /> */}
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
            <Route path="hats/" element={<HatsList />} />
            <Route path="hats/create" element={<HatForm />} />

          </Route>
        </Routes>

      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
