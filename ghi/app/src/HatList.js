import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function HatsList() {
  const [hats, setHats] = useState([]);

  async function loadHats() {
    const response = await fetch("http://localhost:8090/api/hats/");
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  const handleDeleteHat = async (id) => {
  const hatURL = `http://localhost:8090/api/hats/${id}`;
  const response = await fetch(hatURL, { method: "DELETE"});
  if (response.ok) {
    setHats(hats.filter((hat) => hat.id !== id));

  }
}
  useEffect(() => {
    loadHats();
  }, []);

  return (
    <>
      <div>
      <Link to="/hats/create" className="btn btn-primary mb-3">Create A New Hat</Link>
      </div>

      <div class="card" style={{ width: "18rem" }}>
        {hats.map((hat) => (
          <div key={hat.id}>
            <img src={hat.picture} className="card-img-top" alt="Hat" />
            <div class="card-body">
              <h5 class="card-title">{hat.style}</h5>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <strong>Fabric:</strong> {hat.fabric}
              </li>
              <li class="list-group-item">
                <strong>Color:</strong> {hat.color}
              </li>
            </ul>
            <button onClick={() => handleDeleteHat(hat.id)} className="btn btn-lg btn-primary">Delete</button>
          </div>
        ))}
      </div>
    </>
  );
}
