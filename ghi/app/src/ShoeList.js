import React from 'react'
import './ShoeList.css'
import ShoeForm from './ShoeForm'




function ShoeList(props) {
    // if (props.shoes === undefined) {
    //     return null;
    //   }
    const deleteShoe=((id)=>{
        if(window.confirm("Do you want to delete this shoe?")){
            fetch("http://localhost:8080/api/shoes/"+id,
            {method:"Delete"}).then(()=>{
                window.location.reload();

            }).catch((err)=>{
                console.log(err.message)
            })
        }
    })

    return (
        <>

        {/* <form action="action_page.php" method="post"> */}
        <div className="container">


          <ShoeForm />
        </div>
        {/* </form> */}

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model Name</th>
              <th>Manufacturer</th>
              <th>Color</th>
              <th>Picture</th>
              <th>bin</th>
              <th>delete</th>
            </tr>
          </thead>
          <tbody>
            {props.shoes?.map(shoe => {
                return (
              <tr key={shoe.id}>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td><img src={ shoe.picture_url } /></td>
                <td>{ shoe.bin }</td>
                <td><button className="btn btn-danger"
                onClick={()=>{deleteShoe(shoe.id)}}>Delete</button></td>
              </tr>
                );
            })}

          </tbody>
        </table>
        </>
    );
  }
export default ShoeList
